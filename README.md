# GitLab Basics & first steps with CI/CD to fix and build the Go Tanuki

This repository is a template for CI/CD workshops and [documented in the Developer Advocacy handbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/projects/).

## Content

The [slides](https://docs.google.com/presentation/d/e/2PACX-1vQzG8ayO98Z9NTcVbmEsjwQQ0jIYenS6uBJQ34hb-HQK7ALA_ek3ie5jtPa6Jl7tD68erYzszDH-aJ3/pub?start=false&loop=false&delayms=3000) provide the step-by-step instructions as exercises for the [GitLab CI/CD Go Tanuki workshop](https://gitlab.com/gitlab-da/workshops/gitlab-cicd-go-tanuki-workshop).

- First Steps with GitLab
- CI/CD: Getting Started
- CI/CD: Go Project and Tests
- Security with GitLab
- Deliver and Deploy
- What's next
- Exercises for async practice
- Efficiency practice

### Lastest Recording

[![](http://img.youtube.com/vi/kTNfi5z6Uvk/0.jpg)](http://www.youtube.com/watch?v=kTNfi5z6Uvk "1. Swiss Meetup 2021 in January")

## Meetups and Workshops

Meetup workshops are available in the [workshops group](https://gitlab.com/gitlab-da/workshops) in the GitLab Developer Evangelism group. Note that the content, exercises and source code evolved over time :)

### Basics and CI/CD

- [1. Swiss Meetup 2021 January](https://gitlab.com/gitlab-da/workshops/swiss-meetup-2021-jan)
- [Morehouse College CI CD Lecture](https://gitlab.com/gitlab-da/workshops/morehouse-college-lecture-cicd)
- [CI Community Day 2020](https://gitlab.com/gitlab-da/workshops/ci-community-day-2020)
- [KDE Akademy Workshop 2020](https://gitlab.com/gitlab-da/workshops/kde-akademy-workshop-2020)
- [LA / SoCal / Orange County Meetup 2020](https://gitlab.com/gitlab-da/workshops/meetup-2020-cw41)
- [Collision from Home GitLab CI workshop](https://gitlab.com/gitlab-da/workshops/collision-from-home-2020)
- [Linuxing London Meetup - GitLab Introduction](https://gitlab.com/gitlab-da/workshops/meetup-2020-cw17)
- [First virtual GitLab meetup - intro to CI](https://gitlab.com/gitlab-da/workshops/meetup-2020-cw13)

German language:

- [German CI/CD Workshop 2021](https://gitlab.com/gitlab-da/workshops/german-cicd-workshop-2021-jan)

### Monitoring and Security

- [🦊 Identify, analyze, action! Deep monitoring with CI](https://gitlab.com/gitlab-da/workshops/ci-monitoring-webcast-2020)
- [GitLab CI Security Webcast](https://gitlab.com/gitlab-da/workshops/ci-security-webcast-2020)
